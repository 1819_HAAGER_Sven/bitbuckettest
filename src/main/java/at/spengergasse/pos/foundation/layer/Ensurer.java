package at.spengergasse.pos.foundation.layer;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Alexander on 2016-11-16.
 */
public final class Ensurer {
        public static <T> T ensureNotNull(T object) {
            return ensureNotNull(object, "argument");
        }
        public static <T> T ensureNotNull(T object, String objectName) {
            if (object == null) {
                throw new IllegalArgumentException(String.format("%s must not be null!",
                        objectName));
            }
            return object;
        }
        public static String ensureNotBlank(String object, String objectName) {
            if (StringUtils.isBlank(object)) {
                throw new IllegalArgumentException(String.format("%s must not be null " +
                        "empty or blank!", objectName));
            }
            return object;
        }
}
